const fs = require('fs-extra');
const csv = require('csv');
const utils = require('util');

const db = require('../models/index');
const { removeTableNameFromAttributes } = require('../utils');

const csvParse = utils.promisify(csv.parse);

const UserModel = db.user;
const WasherModel = db.washer;
const QueueModel = db.queue;

const { SECRET_KEY } = process.env;

class AdminController {
  static async Login(ctx) {
    const { token } = ctx.request.body;

    if (!token) {
      ctx.throw(400, 'Токен не установлен');
    }

    if (token !== SECRET_KEY) {
      ctx.throw(401, 'Не правильный токен');
    }

    ctx.status = 200;
  }

  static async fetchLog(ctx) {
    const log = removeTableNameFromAttributes(await QueueModel.findAll({
      attributes: [['date', 'reservedOn'], ['createdAt', 'reservedAt']],
      include: [
        { model: UserModel, attributes: [['fullName', 'name'], 'room'] },
        { model: WasherModel, attributes: [['title', 'washer']] },
      ],
      raw: true,
    }));

    ctx.body = { log };
  }

  static async fetchUsers(ctx) {
    const users = removeTableNameFromAttributes(await UserModel.findAll({
      attributes: ['id', ['fullName', 'name'], 'email', 'room', 'points'],
      raw: true,
    }));

    ctx.body = { users };
  }

  static async setUserPoints(ctx) {
    const { id, points } = ctx.request.body;

    console.log(id, points);

    if (id === undefined || points === undefined) {
      ctx.throw(400, 'id или количество бонусов не задано');
    }

    await UserModel.update({ points }, { where: { id } });

    ctx.status = 200;
  }

  static async getFileFromUser(ctx) {
    const patchFile = ctx.request.files['dump.csv'];

    if (patchFile === undefined) ctx.throw(400, 'No file');

    const patchCSV = (await fs.readFile(patchFile.path)).toString('utf-8');

    const patchObject = (await csvParse(patchCSV))
      .slice(1)
      .map(e => ({ id: e[6], fullName: e[4], room: e[5] }));

    for (const updatedUser of patchObject) {
      const user = await UserModel.findOne({ where: { id: updatedUser.id } });

      console.log(updatedUser.fullName);

      if (!user) {
        await UserModel.create({
          id: updatedUser.id,
          fullName: updatedUser.fullName,
          email: '',
          password: '',
          room: updatedUser.room,
          points: 0,
        });
      } else {
        await UserModel.update(updatedUser, { where: { id: updatedUser.id } });
      }
    }

    ctx.status = 200;
  }
}

module.exports = AdminController;
